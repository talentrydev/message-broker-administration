up:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker-administration up -d
down:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker-administration down
test:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker-administration exec php-cli vendor/bin/phpunit
cs:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker-administration exec php-cli vendor/bin/phpcs --standard=PSR12 src tests
cs-fix:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker-administration exec php-cli vendor/bin/phpcbf --standard=PSR12 src tests
deps:
	docker run -v $(shell pwd):/app --rm -t composer install --ignore-platform-reqs
migrate:
	docker-compose -f infrastructure/dev/docker-compose.yml -p message-broker-administration exec php-cli vendor/bin/doctrine-migrations migrations:migrate
