# Message Broker Administration

This library functions as an add-on to [talentrydev/message-broker](https://gitlab.com/talentrydev/message-broker)
library. It deals with "administrative" tasks, such as viewing the contents of the dead letter queue,
replaying or deleting messages from DLQ and sending a summary of the DLQ via slack. This functionality
was removed from the talentrydev/message-broker library in order to keep it lean.

Given the complexity of the class hierarchy, it's recommended to use the
[symfony bundle](https://gitlab.com/talentrydev/message-broker-administration-bundle)
instead of using this library directly.

## Development

- Run `make up` to spin up docker containers
- Run `make deps` to install composer dependencies
- Run `make tests` to run PHPUnit tests