<?php

/**
 * This file is used by doctrine-migrations binary to load DB configuration.
 * It has to be in the root directory, as there's no way to specify its location when executing the binary.
 */

require __DIR__ . '/vendor/autoload.php';

use Doctrine\DBAL\DriverManager;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\ExistingConfiguration;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMSetup;
use Doctrine\Migrations\Metadata\Storage\TableMetadataStorageConfiguration;

$migrationConfig = new Configuration();
$migrationConfig->addMigrationsDirectory(
    'Talentry\MessageBrokerAdministration\Infrastructure\DoctrineMigrations',
    __DIR__ . '/src/Infrastructure/DoctrineMigrations',
);
$migrationConfig->setMetadataStorageConfiguration(new TableMetadataStorageConfiguration());
$migrationConfigLoader = new ExistingConfiguration($migrationConfig);

$paths = [__DIR__ . '/src/Domain/Entity'];
$isDevMode = true;

$ORMConfig = ORMSetup::createAttributeMetadataConfiguration($paths, $isDevMode);
$connection = DriverManager::getConnection([
    'driver' => 'pdo_mysql',
    'host' => 'mariadb',
    'user' => 'root',
    'password' => 'password',
    'dbname' => 'mba',
]);

$entityManager = new EntityManager($connection, $ORMConfig);

return DependencyFactory::fromEntityManager($migrationConfigLoader, new ExistingEntityManager($entityManager));
