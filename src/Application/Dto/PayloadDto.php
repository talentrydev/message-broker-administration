<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Dto;

use JMS\Serializer\Annotation\Type;

class PayloadDto
{
    #[Type('string')]
    public string $id;

    #[Type('string')]
    public string $payload;

    #[Type('string')]
    public string $kibanaLink;

    public function __construct(
        string $id,
    ) {
        $this->id = $id;
    }
}
