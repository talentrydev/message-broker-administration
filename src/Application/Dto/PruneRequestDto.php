<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Dto;

use Talentry\MessageBrokerAdministration\Application\Validator\PruneRequestConstraint;
use JMS\Serializer\Annotation\Type;

#[PruneRequestConstraint]
class PruneRequestDto
{
    #[Type('string')]
    public ?string $operation = null;

    /**
     * @var string[]
     */
    #[Type('array<string>')]
    public ?array $channels = null;

    /**
     * @var string[]
     */
    #[Type('array<string>')]
    public ?array $types = null;

    #[Type('string')]
    public ?string $payloadId = null;

    #[Type('bool')]
    public bool $postSummaryToSlack = false;

    #[Type('bool')]
    public bool $skipInvalid = false;
}
