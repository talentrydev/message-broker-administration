<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Dto;

use JMS\Serializer\Annotation\Type;

class SummaryDto
{
    #[Type('string')]
    public string $channel;

    #[Type('int')]
    public int $total;

    /**
     * @var array<string,int>
     */
    #[Type('array<string,int>')]
    public array $perTenant;

    /**
     * @var array<string,int>
     */
    #[Type('array<string,int>')]
    public array $perChannel;

    /**
     * @var array<string,int>
     */
    #[Type('array<string,int>')]
    public array $perType;

    /**
     * @var PayloadDto[]
     */
    #[Type('array<Talentry\MessageBrokerAdministration\Application\Dto\PayloadDto>')]
    public array $payloads;
}
