<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Mapper;

use Talentry\MessageBrokerAdministration\Application\Dto\PayloadDto;
use Talentry\MessageBrokerAdministration\Application\Dto\SummaryDto;
use Talentry\MessageBrokerAdministration\Domain\Reporting\Summary;

class SummaryDtoMapper
{
    public function fromSummary(Summary $summary): SummaryDto
    {
        $dto = new SummaryDto();
        $dto->channel = $summary->dlqChannel()->getName();
        $dto->total = $summary->total();
        $dto->perType = $summary->perType();
        $dto->perChannel = $summary->perChannel();
        $dto->perTenant = $summary->perTenant();

        $payloads = [];
        foreach ($summary->payloads() as $payload) {
            $payloadDto = new PayloadDto($payload->id);
            if (is_string($payload->payload())) {
                $payloadDto->payload = $payload->payload();
            } else {
                $payloadDto->payload = print_r($payload->payload(), true);
            }

            $payloadDto->kibanaLink = $payload->kibanaLink();

            $payloads[] = $payloadDto;
        }

        $dto->payloads = $payloads;

        return $dto;
    }
}
