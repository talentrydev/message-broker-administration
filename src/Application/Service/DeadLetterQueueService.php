<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Service;

use InvalidArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Talentry\MessageBroker\Application\Exception\ValidationException;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Application\Dto\PruneRequestDto;
use Talentry\MessageBrokerAdministration\Application\Dto\SummaryDto;
use Talentry\MessageBrokerAdministration\Application\Mapper\SummaryDtoMapper;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningOperation;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningService;
use Talentry\MessageBrokerAdministration\Domain\Reporting\SummaryGenerator;
use Talentry\Slack\SlackClient;

class DeadLetterQueueService
{
    public function __construct(
        private readonly PruningService $pruningService,
        private readonly SlackClient $slackClient,
        private readonly SummaryGenerator $summaryGenerator,
        private readonly ValidatorInterface $validator,
        private readonly SummaryDtoMapper $summaryDtoMapper,
        private readonly ChannelRegistry $channelRegistry,
    ) {
    }

    /**
     * @throws ValidationException
     */
    public function pruneDlqChannel(
        PruneRequestDto $pruneRequestDto,
        string $dlqChannelName,
        ?int $limit = null,
    ): SummaryDto {
        $dlqChannel = new Channel($dlqChannelName);

        $violations = $this->validator->validate($pruneRequestDto);
        if ($violations->count() > 0) {
            throw new ValidationException($violations);
        }

        $pruningOperationString = $pruneRequestDto->operation;
        if ($pruningOperationString === null) {
            throw new InvalidArgumentException('Pruning operation must be specified');
        }

        $pruningOperation = PruningOperation::from($pruningOperationString);
        $remainingMessages = [];
        if ($pruneRequestDto->payloadId !== null) {
            $remainingMessages = $this->pruningService->pruneByPayloadId(
                payloadId: $pruneRequestDto->payloadId,
                operation: $pruningOperation,
                dlqChannel: $dlqChannel,
                limit: $limit,
            );
        } elseif ($pruneRequestDto->types !== null) {
            $remainingMessages = $this->pruningService->pruneByType(
                types: $pruneRequestDto->types,
                operation: $pruningOperation,
                dlqChannel: $dlqChannel,
                limit: $limit,
            );
        } elseif ($pruneRequestDto->channels !== null) {
            $originChannels = array_map(
                fn (string $channelName): Channel => new Channel($channelName),
                $pruneRequestDto->channels,
            );
            $remainingMessages = $this->pruningService->pruneByChannel(
                originChannels: $originChannels,
                operation: $pruningOperation,
                dlqChannel: $dlqChannel,
                limit: $limit,
            );
        }

        $summary = $this->summaryGenerator->forMessages(
            $remainingMessages,
            $dlqChannel,
        );

        if ($pruneRequestDto->postSummaryToSlack) {
            $summary->postToSlack($this->slackClient);
        }

        return $this->summaryDtoMapper->fromSummary($summary);
    }

    /**
     * @throws ValidationException
     * @return SummaryDto[]
     */
    public function pruneAllDlqChannels(PruneRequestDto $pruneRequestDto, ?int $limit = null): array
    {
        $summaries = [];
        foreach ($this->channelRegistry->getAllDeadLetterQueueNames() as $dlq) {
            $summaries[] = $this->pruneDlqChannel($pruneRequestDto, $dlq, $limit);
        }

        return $summaries;
    }

    public function getSummary(
        string $dlqChannelName,
        bool $includePerTenantSummary = true,
        ?int $limit = null,
    ): SummaryDto {
        return $this->generateSummaryForDlqChannel($dlqChannelName, $includePerTenantSummary, $limit);
    }

    /**
     * @return SummaryDto[]
     */
    public function getSummariesForAllDlqChannels(
        bool $includePerTenantSummary = true,
        ?int $limit = null,
    ): array {
        $summaries = [];
        foreach ($this->channelRegistry->getAllDeadLetterQueueNames() as $dlqChannel) {
            $summaries[] = $this->generateSummaryForDlqChannel($dlqChannel, $includePerTenantSummary, $limit);
        }

        return $summaries;
    }

    private function generateSummaryForDlqChannel(
        string $dlqChannelName,
        bool $includePerTenantSummary,
        ?int $limit = null,
    ): SummaryDto {
        $dlqChannel = new Channel($dlqChannelName);
        $summary = $this->summaryGenerator->forDlqChannel($dlqChannel, $includePerTenantSummary, $limit);

        return $this->summaryDtoMapper->fromSummary($summary);
    }
}
