<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Validator;

use Attribute;
use Symfony\Component\Validator\Constraint;

#[Attribute(Attribute::TARGET_CLASS)]
class PruneRequestConstraint extends Constraint
{
    public function validatedBy(): string
    {
        return $this::class . 'Validator';
    }

    public function getTargets(): string
    {
        return self::CLASS_CONSTRAINT;
    }
}
