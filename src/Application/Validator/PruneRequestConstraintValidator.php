<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Application\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Talentry\MessageBrokerAdministration\Application\Dto\PruneRequestDto;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningOperation;

class PruneRequestConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof PruneRequestConstraint) {
            throw new UnexpectedTypeException($constraint, PruneRequestConstraint::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) to take care of that
        if ($value === null || $value === '') {
            return;
        }

        if (!$value instanceof PruneRequestDto) {
            throw new UnexpectedValueException($value, PruneRequestDto::class);
        }

        if (count(array_filter([$value->payloadId, $value->channels, $value->types])) !== 1) {
            $this->context
                ->buildViolation('You must specify either payloadId, types or channels.')
                ->addViolation()
            ;
        }

        $operation = $value->operation === null ? null : PruningOperation::tryFrom($value->operation);
        if ($operation === null) {
            $supportedValues = join(
                ', ',
                array_map(fn (PruningOperation $operation): string => $operation->value, PruningOperation::cases())
            );
            $this->context
                ->buildViolation('Invalid pruning operation. Supported values are: ' . $supportedValues)
                ->atPath('operation')
                ->addViolation()
            ;
        }

        if ($value->types !== null && !$value->skipInvalid) {
            foreach ($value->types as $type) {
                if (!class_exists($type)) {
                    $this->context
                        ->buildViolation('Invalid type provided: ' . $type)
                        ->atPath('types')
                        ->addViolation();
                }
            }
        }
    }
}
