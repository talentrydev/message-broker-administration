<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\ApplicationInterface;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\Message\UnsupportedMessageException;

interface MessageRequeuer
{
    /**
     * @throws UnsupportedMessageException
     */
    public function requeue(Message $message): void;
    public function supports(Message $message): bool;
}
