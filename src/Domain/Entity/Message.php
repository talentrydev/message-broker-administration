<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'message_broker_administration_message', options: ['collate' => 'utf8mb4_bin'])]
#[ORM\Entity]
class Message
{
    #[ORM\Id]
    #[ORM\Column(name: 'payload_id', type: 'string')]
    private string $payloadId;

    #[ORM\Column(name: 'payload', type: 'text')]
    private string $payload;

    #[ORM\Column(name: 'channel', type: 'string')]
    private string $channel;

    #[ORM\Column(name: 'dlq_channel', type: 'string')]
    private string $dlqChannel;

    #[ORM\Column(name: 'type', type: 'string', nullable: true)]
    private ?string $type;

    public function __construct(
        string $payloadId,
        string $payload,
        string $channel,
        string $dlqChannel,
        ?string $type = null,
    ) {
        $this->payloadId = $payloadId;
        $this->payload = $payload;
        $this->channel = $channel;
        $this->dlqChannel = $dlqChannel;
        $this->type = $type;
    }

    public function getPayloadId(): string
    {
        return $this->payloadId;
    }

    public function getPayload(): string
    {
        return $this->payload;
    }

    public function getChannel(): string
    {
        return $this->channel;
    }

    public function getDlqChannel(): string
    {
        return $this->dlqChannel;
    }

    public function getType(): ?string
    {
        return $this->type;
    }
}
