<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Mapper;

use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message as MessageEntity;

class MessageMapper
{
    public function mapMessageEntityToMessage(MessageEntity $messageEntity): Message
    {
        return new Message(
            new Channel($messageEntity->getChannel()),
            $messageEntity->getPayload(),
            $messageEntity->getPayloadId(),
        );
    }
}
