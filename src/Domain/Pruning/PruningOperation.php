<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Pruning;

enum PruningOperation: string
{
    case REQUEUE = 'requeue';
    case DELETE = 'delete';
}
