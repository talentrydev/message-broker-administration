<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Pruning;

use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Talentry\MessageBroker\ApplicationInterface\Message\UnsupportedMessageException;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBrokerAdministration\ApplicationInterface\MessageRequeuer;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Domain\Mapper\MessageMapper;
use Talentry\MessageBrokerAdministration\Infrastructure\Repository\MessageRepository;

class PruningService
{
    public function __construct(
        private readonly MessageRepository $messageRepository,
        private readonly MessageRequeuer $messageRequeuer,
        private readonly MessageMapper $messageMapper,
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    /**
     * @return Message[] - remaining messages in the queue
     */
    public function pruneByPayloadId(
        string $payloadId,
        PruningOperation $operation,
        ?Channel $dlqChannel = null,
        ?int $limit = null,
    ): array {
        $message = $this->messageRepository->findByPayloadId($payloadId);
        if ($message !== null) {
            $this->pruneMessages([$message], $operation);
        }

        return $this->getAllMessagesInDlq($dlqChannel, $limit);
    }

    /**
     * @param Channel[] $originChannels - Channels that will be used as a filter for pruning operation
     *      (i.e. channels from which the messages originated before ending up in DLQ)
     * @return Message[] - remaining messages in the queue
     */
    public function pruneByChannel(
        array $originChannels,
        PruningOperation $operation,
        ?Channel $dlqChannel = null,
        ?int $limit = null,
    ): array {
        $channelNames = array_map(fn (Channel $channel): string => $channel->getName(), $originChannels);
        $messages = $this->messageRepository->findByOriginChannel($channelNames, $limit);
        $this->pruneMessages($messages, $operation);

        return $this->getAllMessagesInDlq($dlqChannel, $limit);
    }

    /**
     * @return Message[] - remaining messages in the queue
     */
    public function pruneByType(
        array $types,
        PruningOperation $operation,
        ?Channel $dlqChannel = null,
        ?int $limit = null,
    ): array {
        $messages = $this->messageRepository->findByType($types, $limit);
        $this->pruneMessages($messages, $operation);

        return $this->getAllMessagesInDlq($dlqChannel, $limit);
    }

    /**
     * @return Message[]
     */
    private function getAllMessagesInDlq(Channel $dlqChannel, ?int $limit = null): array
    {
        return $this->messageRepository->findByDlqChannel($dlqChannel->getName(), $limit);
    }

    /**
     * @param Message[] $messages
     */
    private function pruneMessages(array $messages, PruningOperation $operation): void
    {
        try {
            foreach ($messages as $message) {
                if ($operation === PruningOperation::REQUEUE) {
                    $this->messageRequeuer->requeue($this->messageMapper->mapMessageEntityToMessage($message));
                }
                $this->entityManager->remove($message);
            }

            $this->entityManager->flush();
        } catch (UnsupportedMessageException $e) {
            //this should never happen, considering there is a FallbackMessageParser
            throw new RuntimeException('Pruning service is not correctly configured', $e->getCode(), $e);
        }
    }
}
