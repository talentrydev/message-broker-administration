<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Reporting;

class Payload
{
    public function __construct(
        public string $id,
        private readonly string|object $payload,
        private readonly string $kibanaLink,
    ) {
    }

    public function payload(): string|object
    {
        return $this->payload;
    }

    public function kibanaLink(): string
    {
        return $this->kibanaLink;
    }
}
