<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Reporting;

use Exception;
use RuntimeException;
use SplFileInfo;
use Talentry\Backoff\BackoffDecorator;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\Slack\SlackClient;

class Summary
{
    /**
     * @param array<string,int> $perChannel
     * @param array<string,int> $perType
     * @param array<string,int> $perTenant
     * @param Payload[] $payloads
     */
    public function __construct(
        private readonly Channel $dlqChannel,
        private readonly int $total,
        private readonly array $perChannel,
        private readonly array $perType,
        private readonly array $perTenant,
        private readonly array $payloads
    ) {
    }

    public function dlqChannel(): Channel
    {
        return $this->dlqChannel;
    }

    public function total(): int
    {
        return $this->total;
    }

    /**
     * @return array<string,int>
     */
    public function perChannel(): array
    {
        return $this->perChannel;
    }

    public function forChannel(string $channel): int
    {
        return $this->perChannel[$channel] ?? 0;
    }

    /**
     * @return array<string,int>
     */
    public function perType(): array
    {
        return $this->perType;
    }

    public function forType(string $type): int
    {
        return $this->perType[$type] ?? 0;
    }

    /**
     * @return array<string,int>
     */
    public function perTenant(): array
    {
        return $this->perTenant;
    }

    public function forTenant(string $tenant): int
    {
        return $this->perTenant[$tenant] ?? 0;
    }

    /**
     * @return Payload[]
     */
    public function payloads(): array
    {
        return $this->payloads;
    }

    public function __toString(): string
    {
        $summary = "SUMMARY FOR DLQ CHANNEL {$this->dlqChannel()->getName()}:\n";
        $summary .= "TOTAL: {$this->total()}\n";

        if (count($this->perChannel()) > 0) {
            $summary .= "PER CHANNEL:\n";
            foreach ($this->perChannel() as $channel => $count) {
                $summary .= "\t$channel: $count\n";
            }
        }

        if (count($this->perType()) > 0) {
            $summary .= "PER TYPE:\n";
            foreach ($this->perType() as $type => $count) {
                $summary .= "\t$type: $count\n";
            }
        }

        if (count($this->perTenant()) > 0) {
            $summary .= "PER TENANT:\n";
            foreach ($this->perTenant() as $tenant => $count) {
                $summary .= "\t$tenant: $count\n";
            }
        }

        return $summary;
    }

    public function dumpPayloadsToFile(?SplFileInfo $file = null): SplFileInfo
    {
        if ($file === null) {
            $filePath = tempnam(sys_get_temp_dir(), '');
            if ($filePath === false) {
                throw new RuntimeException('Error creating temp file');
            }

            $file = new SplFileInfo($filePath);
        }

        $fileObject = $file->openFile('a');
        foreach ($this->payloads() as $payload) {
            $fileObject->fwrite(print_r($payload, true));
        }

        return $file;
    }

    public function postToSlack(SlackClient $slackClient, ?string $slackChannelName = null, int $retries = 0): void
    {
        $slackChannel = null;
        if ($slackChannelName !== null) {
            $slackChannel = $slackClient->getChannel($slackChannelName);
            if ($slackChannel === null) {
                throw new Exception('Invalid slack channel specified: ' . $slackChannelName);
            }
        }

        $slackClientWithBackoff = BackoffDecorator::maxAttempts($retries)->decorate($slackClient);
        $message = (string) $this;
        if ($this->total() > 0) {
            $slackClientWithBackoff->sendFile($this->dumpPayloadsToFile(), $slackChannel, $message);
        }
    }
}
