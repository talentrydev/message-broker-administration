<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Reporting;

use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Domain\Mapper\MessageMapper;
use Talentry\MessageBrokerAdministration\Infrastructure\Repository\MessageRepository;

class SummaryGenerator
{
    public function __construct(
        private readonly MessageParser $messageParser,
        private readonly MessageRepository $messageRepository,
        private readonly MessageMapper $messageMapper = new MessageMapper(),
    ) {
    }

    public function forDlqChannel(
        Channel $dlqChannel,
        bool $includePerTenantSummary = true,
        ?int $limit = null,
    ): Summary {
        $messages = $this->messageRepository->findByDlqChannel($dlqChannel->getName(), $limit);

        return $this->forMessages($messages, $dlqChannel, $includePerTenantSummary);
    }

    /**
     * @param Message[] $messages
     */
    public function forMessages(array $messages, Channel $dlqChannel, bool $includePerTenantSummary = true): Summary
    {
        $total = count($messages);
        /** @var array<string,int> $perChannel */
        $perChannel = [];
        /** @var array<string,int> $perType */
        $perType = [];
        /** @var array<string,int> $perTenant */
        $perTenant = [];
        /** @var array<Payload> $payloads */
        $payloads = [];

        foreach ($messages as $messageEntity) {
            $message = $this->messageMapper->mapMessageEntityToMessage($messageEntity);
            $tenantId = $this->messageParser->getTenantId($message) ?? 'n/a';
            $type = $this->messageParser->getMessageType($message);
            $messageChannel = $message->getChannel()->getName();
            $payloads[] = new Payload(
                id: $message->getPayloadId(),
                payload: $this->messageParser->parse($message),
                kibanaLink: $this->messageParser->getKibanaLink($message),
            );

            if (!isset($perChannel[$messageChannel])) {
                $perChannel[$messageChannel] = 0;
            }
            $perChannel[$messageChannel]++;

            if (!isset($perType[$type])) {
                $perType[$type] = 0;
            }
            $perType[$type]++;

            if ($includePerTenantSummary) {
                if (!isset($perTenant[$tenantId])) {
                    $perTenant[$tenantId] = 0;
                }
                $perTenant[$tenantId]++;
            }
        }

        arsort($perChannel, SORT_NUMERIC);
        arsort($perType, SORT_NUMERIC);
        arsort($perTenant, SORT_NUMERIC);

        return new Summary(
            dlqChannel: $dlqChannel,
            total: $total,
            perChannel: $perChannel,
            perType: $perType,
            perTenant: $perTenant,
            payloads: $payloads
        );
    }
}
