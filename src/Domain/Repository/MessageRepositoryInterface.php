<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Repository;

use Talentry\MessageBrokerAdministration\Domain\Entity\Message;

interface MessageRepositoryInterface
{
    public function findByPayloadId(string $payloadId): ?Message;

    /**
     * @return Message[]
     */
    public function findByDlqChannel(string $dlqChannel, ?int $limit = null): array;

    /**
     * @param string|string[] $originChannels
     * @return Message[]
     */
    public function findByOriginChannel(string|array $originChannels, ?int $limit = null): array;

    /**
     * @param string|string[] $types
     * @return Message[]
     */
    public function findByType(string|array $types, ?int $limit = null): array;
}
