<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Domain\Storage;

use Doctrine\ORM\EntityManagerInterface;
use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\Message\UnsupportedMessageException;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message as MessageEntity;

class MessageStorageService
{
    public function __construct(
        private readonly MessageSubscriber $messageSubscriber,
        private readonly EntityManagerInterface $entityManager,
        private readonly ChannelRegistry $channelRegistry,
        private readonly MessageParser $messageParser,
    ) {
    }

    public function storeMessagesInDlqChannel(Channel $dlqChannel): void
    {
        $unflushedMessages = 0;
        do {
            $message = $this->messageSubscriber->getMessageFromChannel($dlqChannel);
            if ($message !== null) {
                $messageEntity = new MessageEntity(
                    payloadId: $message->getPayloadId(),
                    payload: $message->getPayload(),
                    channel: $message->getChannel()->getName(),
                    dlqChannel: $dlqChannel->getName(),
                    type: $this->getMessageType($message),
                );

                $this->entityManager->persist($messageEntity);
                if (++$unflushedMessages >= 100) {
                    $this->entityManager->flush();
                    $unflushedMessages = 0;
                }
            }
        } while ($message !== null);

        if ($unflushedMessages > 0) {
            $this->entityManager->flush();
        }
    }

    public function storeMessagesInAllDlqChannels(): void
    {
        foreach ($this->channelRegistry->getAllDeadLetterQueueNames() as $dlqName) {
            $this->storeMessagesInDlqChannel(new Channel($dlqName));
        }
    }

    private function getMessageType(Message $message): ?string
    {
        try {
            return $this->messageParser->getMessageType($message);
        } catch (UnsupportedMessageException) {
            return null;
        }
    }
}
