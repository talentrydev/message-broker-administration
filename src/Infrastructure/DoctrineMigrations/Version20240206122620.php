<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Infrastructure\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240206122620 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create message_broker_administration_message table';
    }

    public function up(Schema $schema): void
    {
        $sql = 'CREATE TABLE message_broker_administration_message (payload_id VARCHAR(255) NOT NULL, ';
        $sql .= 'payload LONGTEXT NOT NULL, channel VARCHAR(255) NOT NULL, dlq_channel VARCHAR(255) NOT NULL, ';
        $sql .= 'PRIMARY KEY(payload_id)) DEFAULT CHARACTER SET UTF8mb4 COLLATE `utf8mb4_bin` ENGINE = InnoDB';

        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE message_broker_administration_message');
    }
}
