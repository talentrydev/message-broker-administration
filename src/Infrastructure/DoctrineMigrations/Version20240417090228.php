<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Infrastructure\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240417090228 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add type column to message table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message_broker_administration_message ADD type VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE message_broker_administration_message DROP type');
    }
}
