<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Infrastructure\MessageRequeuer;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBrokerAdministration\ApplicationInterface\MessageRequeuer;

class CompositeMessageRequeuer implements MessageRequeuer
{
    /**
     * @var MessageRequeuer[]
     */
    private array $requeuers = [];

    public function __construct(
        private readonly FallbackMessageRequeuer $fallbackMessageRequeuer,
    ) {
    }

    public function registerRequeuer(MessageRequeuer $requeuer): void
    {
        $this->requeuers[] = $requeuer;
    }

    public function supports(Message $message): bool
    {
        foreach ($this->getRequeuers() as $requeuer) {
            if ($requeuer->supports($message)) {
                return true;
            }
        }

        return false;
    }

    public function requeue(Message $message): void
    {
        foreach ($this->getRequeuers() as $requeuer) {
            if ($requeuer->supports($message)) {
                $requeuer->requeue($message);
                return;
            }
        }
    }

    /**
     * @return MessageRequeuer[]
     */
    private function getRequeuers(): array
    {
        return array_merge($this->requeuers, [$this->fallbackMessageRequeuer]);
    }
}
