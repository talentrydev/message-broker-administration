<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Infrastructure\MessageRequeuer;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBrokerAdministration\ApplicationInterface\MessageRequeuer;

class FallbackMessageRequeuer implements MessageRequeuer
{
    public function __construct(
        private readonly MessagePublisher $messagePublisher,
    ) {
    }

    public function requeue(Message $message): void
    {
        $this->messagePublisher->sendMessage($message);
    }

    public function supports(Message $message): bool
    {
        return true;
    }
}
