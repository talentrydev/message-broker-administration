<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Infrastructure\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Domain\Repository\MessageRepositoryInterface;

class MessageRepository implements MessageRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function findByPayloadId(string $payloadId): ?Message
    {
        return $this
            ->entityManager
            ->createQueryBuilder()
            ->select('m')
            ->from(Message::class, 'm')
            ->where('m.payloadId = :payloadId')
            ->setParameter('payloadId', $payloadId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByDlqChannel(string $dlqChannel, ?int $limit = null): array
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select('m')
            ->from(Message::class, 'm')
            ->where('m.dlqChannel = :dlqChannel')
            ->setParameter('dlqChannel', $dlqChannel)
        ;

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function findByOriginChannel(string|array $originChannels, ?int $limit = null): array
    {
        if (is_string($originChannels)) {
            $originChannels = [$originChannels];
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select('m')
            ->from(Message::class, 'm')
            ->where('m.channel IN (:originChannels)')
            ->setParameter('originChannels', $originChannels)
        ;

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function findByType(string|array $types, ?int $limit = null): array
    {
        if (is_string($types)) {
            $types = [$types];
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb
            ->select('m')
            ->from(Message::class, 'm')
            ->where('m.type IN (:types)')
            ->setParameter('types', $types)
        ;

        if ($limit !== null) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}
