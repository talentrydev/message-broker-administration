<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Presentation\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Domain\Reporting\SummaryGenerator;
use Talentry\Slack\SlackClient;

#[AsCommand(
    name: 'ty:message-broker:channel-summary',
    description: 'Generates a summary for the specified channel (all DLQ channels by default)',
)]
class GenerateChannelSummaryCommand extends Command
{
    private const string DEFAULT_SLACK_CHANNEL = 'general';
    private const int DEFAULT_LIMIT = 1000;
    private const int UPLOAD_FILE_RETRIES = 3; //sometimes slack API returns a 408 when uploading files

    public function __construct(
        private readonly SummaryGenerator $summaryGenerator,
        private readonly SlackClient $slackClient,
        private readonly ChannelRegistry $channelRegistry,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        parent::configure();

        $this
            ->addOption('dlq-channel', null, InputOption::VALUE_REQUIRED, 'DLQ channel name')
            ->addOption('post-to-slack', null, InputOption::VALUE_NONE, 'Post summary to slack')
            ->addOption(
                'slack-channel',
                null,
                InputOption::VALUE_REQUIRED,
                'Slack channel to which summary will be posted',
            )
            ->addOption(
                'without-per-tenant-summary',
                null,
                InputOption::VALUE_NONE,
                'Do not include per-tenant summary'
            )
            ->addOption(
                'limit',
                null,
                InputOption::VALUE_REQUIRED,
                'Limit the number of processed messages (prevents OOM errors)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $dlqChannelName = $input->getOption('dlq-channel');
        if (!is_string($dlqChannelName)) {
            $dlqChannelName = null;
        }

        $postToSlack = $input->getOption('post-to-slack');
        if (!is_bool($postToSlack)) {
            $postToSlack = false;
        }

        $slackChannel = $input->getOption('slack-channel');
        if (!is_string($slackChannel)) {
            $slackChannel = self::DEFAULT_SLACK_CHANNEL;
        }

        $limit = $input->getOption('limit');
        if (!is_int($limit)) {
            $limit = self::DEFAULT_LIMIT;
        }

        if ($postToSlack && $this->slackClient->getChannel($slackChannel) === null) {
            $output->writeln('Invalid slack channel: ' . $slackChannel);

            return self::FAILURE;
        }

        $includePerTenantSummary = true;
        $withoutPerTenantSummary = $input->getOption('without-per-tenant-summary');
        if (is_bool($withoutPerTenantSummary)) {
            $includePerTenantSummary = !$withoutPerTenantSummary;
        }

        if ($dlqChannelName !== null) {
            $dlqChannelNames = [$dlqChannelName];
        } else {
            $dlqChannelNames = $this->channelRegistry->getAllDeadLetterQueueNames();
        }

        foreach ($dlqChannelNames as $dlqChannelName) {
            $summary = $this->summaryGenerator->forDlqChannel(
                dlqChannel: new Channel($dlqChannelName),
                includePerTenantSummary: $includePerTenantSummary,
                limit: $limit,
            );

            if ($postToSlack) {
                $summary->postToSlack($this->slackClient, $slackChannel, self::UPLOAD_FILE_RETRIES);
            } else {
                $output->writeln((string) $summary);
                $output->writeln('Payloads dumped to: ' . $summary->dumpPayloadsToFile()->getRealPath());
            }
        }

        return self::SUCCESS;
    }
}
