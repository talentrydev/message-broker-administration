<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Presentation\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\MessageBrokerAdministration\Domain\Storage\MessageStorageService;

#[AsCommand(
    name: 'ty:message-broker:store-dlq-messages',
    description: 'Stores messages from DLQ queues to the database',
)]
class StoreDlqMessagesCommand extends Command
{
    public function __construct(
        private readonly MessageStorageService $messageStorageService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->messageStorageService->storeMessagesInAllDlqChannels();

        return self::SUCCESS;
    }
}
