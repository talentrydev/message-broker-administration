<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Presentation\Controller;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Talentry\MessageBroker\Application\Exception\ValidationException;
use Talentry\MessageBrokerAdministration\Application\Dto\PruneRequestDto;
use Talentry\MessageBrokerAdministration\Application\Service\DeadLetterQueueService;

class ChannelController
{
    public function __construct(
        private readonly DeadLetterQueueService $deadLetterQueueService,
        private readonly SerializerInterface $serializer,
    ) {
    }

    #[IsGranted("ROLE_ADMIN")]
    public function pruneDlqChannel(Request $request, string $dlqChannelName): Response
    {
        $pruneRequestDto = $this->serializer->deserialize($request->getContent(), PruneRequestDto::class, 'json');
        try {
            $summary = $this->deadLetterQueueService->pruneDlqChannel(
                pruneRequestDto: $pruneRequestDto,
                dlqChannelName: $dlqChannelName,
                limit: $this->getLimitFromRequest($request),
            );

            return new Response($this->serializer->serialize($summary, 'json'));
        } catch (ValidationException $e) {
            return new Response($this->serializer->serialize($e->violations(), 'json'), Response::HTTP_BAD_REQUEST);
        }
    }

    #[IsGranted("ROLE_ADMIN")]
    public function pruneAllDlqChannels(Request $request): Response
    {
        $pruneRequestDto = $this->serializer->deserialize($request->getContent(), PruneRequestDto::class, 'json');
        try {
            $summaries = $this->deadLetterQueueService->pruneAllDlqChannels(
                pruneRequestDto: $pruneRequestDto,
                limit: $this->getLimitFromRequest($request),
            );

            return new Response($this->serializer->serialize($summaries, 'json'));
        } catch (ValidationException $e) {
            return new Response($this->serializer->serialize($e->violations(), 'json'), Response::HTTP_BAD_REQUEST);
        }
    }

    #[IsGranted("ROLE_ADMIN")]
    public function summaryForDlqChannel(Request $request, string $dlqChannelName): Response
    {
        $summary = $this->deadLetterQueueService->getSummary(
            dlqChannelName: $dlqChannelName,
            limit: $this->getLimitFromRequest($request),
        );

        return new Response($this->serializer->serialize($summary, 'json'));
    }

    #[IsGranted("ROLE_ADMIN")]
    public function summariesForAllDlqChannels(Request $request): Response
    {
        $summaries = $this->deadLetterQueueService->getSummariesForAllDlqChannels(
            limit: $this->getLimitFromRequest($request),
        );

        return new Response($this->serializer->serialize($summaries, 'json'));
    }

    private function getLimitFromRequest(Request $request): ?int
    {
        if ($request->query->has('limit') && is_numeric($request->query->get('limit'))) {
            return (int) $request->query->get('limit');
        }

        return null;
    }
}
