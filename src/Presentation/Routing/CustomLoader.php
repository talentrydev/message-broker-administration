<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Presentation\Routing;

use RuntimeException;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Talentry\MessageBrokerAdministration\Presentation\Controller\ChannelController;

class CustomLoader extends Loader
{
    private bool $isLoaded = false;

    public function load($resource, string $type = null): mixed
    {
        if (true === $this->isLoaded) {
            throw new RuntimeException('Do not add the "custom" loader twice');
        }

        $routes = new RouteCollection();

        $this->registerPruneDlqRoute($routes);
        $this->registerPruneAllDlqChannelsRoute($routes);
        $this->registerSummaryForDlqChannelRoute($routes);
        $this->registerSummariesForAllDlqChannelsRoute($routes);

        $this->isLoaded = true;

        return $routes;
    }

    public function supports($resource, string $type = null): bool
    {
        return 'custom' === $type;
    }

    private function registerPruneDlqRoute(RouteCollection $routes): void
    {
        $path = '/prune/{dlqChannelName}';
        $defaults = [
            '_controller' => ChannelController::class . '::pruneDlqChannel',
        ];

        $route = new Route(path: $path, defaults: $defaults, methods: ['POST']);

        $routeName = 'message-broker.prune-dlq-channel';
        $routes->add($routeName, $route);
    }

    private function registerPruneAllDlqChannelsRoute(RouteCollection $routes): void
    {
        $path = '/prune';
        $defaults = [
            '_controller' => ChannelController::class . '::pruneAllDlqChannels',
        ];

        $route = new Route(path: $path, defaults: $defaults, methods: ['POST']);

        $routeName = 'message-broker.prune-all-dlq-channels';
        $routes->add($routeName, $route);
    }

    private function registerSummaryForDlqChannelRoute(RouteCollection $routes): void
    {
        $path = '/summary/{dlqChannelName}';
        $defaults = [
            '_controller' => ChannelController::class . '::summaryForDlqChannel',
        ];

        $route = new Route(path: $path, defaults: $defaults);

        $routeName = 'message-broker.summary-for-dlq-channel';
        $routes->add($routeName, $route);
    }

    private function registerSummariesForAllDlqChannelsRoute(RouteCollection $routes): void
    {
        $path = '/summary';
        $defaults = [
            '_controller' => ChannelController::class . '::summariesForAllDlqChannels',
        ];

        $route = new Route(path: $path, defaults: $defaults);

        $routeName = 'message-broker.summaries-for-all-dlq-channels';
        $routes->add($routeName, $route);
    }
}
