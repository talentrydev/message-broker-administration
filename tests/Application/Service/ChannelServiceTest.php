<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Application\Service;

use Exception;
use SplFileInfo;
use SplStack;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Talentry\MessageBroker\Application\Exception\ValidationException;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Application\Dto\PruneRequestDto;
use Talentry\MessageBrokerAdministration\Application\Dto\SummaryDto;
use Talentry\MessageBrokerAdministration\Application\Mapper\SummaryDtoMapper;
use Talentry\MessageBrokerAdministration\Application\Service\DeadLetterQueueService;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningOperation;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningService;
use Talentry\MessageBrokerAdministration\Domain\Reporting\SummaryGenerator;
use Talentry\MessageBrokerAdministration\Infrastructure\Repository\MessageRepository;
use Talentry\MessageBrokerAdministration\Tests\DatabaseTestCase;
use Talentry\MessageBrokerAdministration\Tests\Mocks\MockMessageParser;
use Talentry\MessageBrokerAdministration\Tests\Mocks\MockPruningService;
use Talentry\Slack\SlackClient;

class ChannelServiceTest extends DatabaseTestCase
{
    private DeadLetterQueueService $deadLetterQueueService;
    private PruningService $pruningService;
    private PruneRequestDto $pruneRequestDto;
    private ChannelRegistry $channelRegistry;
    private string $mockPayload = 'foo';
    private ?Exception $caughtException = null;
    /**
     * @var SummaryDto[]
     */
    private array $summaries = [];
    private string $deadLetterQueueSuffix = '_dead_letter_queue';

    protected function setUp(): void
    {
        parent::setUp();

        $this->pruningService = new MockPruningService();
        $this->channelRegistry = new ChannelRegistry($this->deadLetterQueueSuffix);
        $messageRepository = $this->getService(MessageRepository::class);
        $this->deadLetterQueueService = new DeadLetterQueueService(
            $this->pruningService,
            $this->createMock(SlackClient::class),
            new SummaryGenerator(
                new MockMessageParser($this->mockPayload),
                $messageRepository,
            ),
            self::getContainer()->get(ValidatorInterface::class),
            new SummaryDtoMapper(),
            $this->channelRegistry,
        );

        $this->pruneRequestDto = new PruneRequestDto();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        if ($this->caughtException !== null) {
            throw $this->caughtException;
        }
    }

    public function testRequeueByPayloadId(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithRequeueOperation();
        $this->givenPruneRequestWithPayloadId();
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectPayloadIdToBeRequeued();
        $this->expectSummary($channel);
    }

    public function testDeleteByPayloadId(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithDeleteOperation();
        $this->givenPruneRequestWithPayloadId();
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectPayloadIdToBeDeleted();
        $this->expectSummary($channel);
    }

    public function testRequeueWithValidChannels(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithRequeueOperation();
        $this->givenPruneRequestWithAllRegisteredChannels();
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectChannelsToBeRequeued();
        $this->expectSummary($channel);
    }

    public function testDeleteWithValidChannels(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithDeleteOperation();
        $this->givenPruneRequestWithAllRegisteredChannels();
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectChannelsToBeDeleted();
        $this->expectSummary($channel);
    }

    public function testRequeueWithValidTypes(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithRequeueOperation();
        $this->givenPruneRequestWithTypes([SplFileInfo::class, SplStack::class]);
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectTypesToBeRequeued();
        $this->expectSummary($channel);
    }

    public function testDeleteWithValidTypes(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithDeleteOperation();
        $this->givenPruneRequestWithTypes([SplFileInfo::class, SplStack::class]);
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectTypesToBeDeleted();
        $this->expectSummary($channel);
    }

    public function testRequeueWithInvalidTypes(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithRequeueOperation();
        $this->givenPruneRequestWithTypes(['foo', 'bar']);
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectValidationException();
    }

    public function testDeleteWithInvalidTypes(): void
    {
        $channel = $this->givenChannel();
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->givenPruneRequestWithDeleteOperation();
        $this->givenPruneRequestWithTypes(['foo', 'bar']);
        $this->whenPruneRequestIsMadeForChannel($channel);
        $this->expectValidationException();
    }

    public function testGetSummary(): void
    {
        $channel = $this->givenChannel();
        $dlqChannel = new Channel($this->channelRegistry->getDeadLetterQueueNameForChannel($channel));
        $message = $this->givenMessage($channel);
        $this->givenThatMessageIsStored($message);
        $this->whenSummaryIsGeneratedForChannel($dlqChannel);
        $this->expectSummary($dlqChannel, 1);
    }

    public function testGetSummaryForAllDlqChannels(): void
    {
        $channel1 = $this->givenChannel('first');
        $channel2 = $this->givenChannel('second');
        $message1 = $this->givenMessage($channel1);
        $message2 = $this->givenMessage($channel2);
        $this->givenThatMessageEndsUpInDlq($message1);
        $this->givenThatMessageEndsUpInDlq($message2);
        $this->whenSummariesAreGeneratedForAllDlqChannels();
        $this->expectSummary($this->getDlq($channel1->getName()), 1);
        $this->expectSummary($this->getDlq($channel2->getName()), 1);
    }

    public function testPruneAllDlqChannels(): void
    {
        $channel1 = $this->givenChannel('first');
        $channel2 = $this->givenChannel('second');
        $message1 = $this->givenMessage($channel1);
        $message2 = $this->givenMessage($channel2);
        $this->givenThatMessageEndsUpInDlq($message1);
        $this->givenThatMessageEndsUpInDlq($message2);
        $this->givenPruneRequestWithRequeueOperation();
        $this->givenPruneRequestWithPayloadId();
        $this->whenPruneRequestIsMadeForAllDlqChannels();
        $this->expectPayloadIdToBeRequeued();
        $this->expectSummary($this->getDlq($channel1->getName()));
        $this->expectSummary($this->getDlq($channel2->getName()));
    }

    private function givenPruneRequestWithRequeueOperation(): void
    {
        $this->pruneRequestDto->operation = PruningOperation::REQUEUE->value;
    }

    private function givenPruneRequestWithDeleteOperation(): void
    {
        $this->pruneRequestDto->operation = PruningOperation::DELETE->value;
    }

    private function givenPruneRequestWithPayloadId(): void
    {
        $this->pruneRequestDto->payloadId = 'foo';
    }

    private function givenPruneRequestWithAllRegisteredChannels(): void
    {
        $channels = array_map(
            fn (Channel $channel): string => $channel->getName(),
            $this->channelRegistry->getChannels(),
        );

        $this->pruneRequestDto->channels = $channels;
    }

    private function givenPruneRequestWithTypes(array $types = ['foo', 'bar']): void
    {
        $this->pruneRequestDto->types = $types;
    }

    private function givenChannel(string $channelName = 'channel'): Channel
    {
        $channel = new Channel($channelName);
        $this->channelRegistry->registerChannel($channel);

        return $channel;
    }

    private function givenMessage(Channel $channel): Message
    {
        $dlqChannel = $this->channelRegistry->getDeadLetterQueueNameForChannel($channel);

        return new Message(uniqid(), $this->mockPayload, $channel->getName(), $dlqChannel);
    }

    private function givenThatMessageIsStored(Message $message): void
    {
        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }

    private function givenThatMessageEndsUpInDlq(Message $message): void
    {
        $dlq = $this->getDlq($message->getChannel());
        $message = new Message(
            $message->getPayloadId(),
            $message->getPayload(),
            $message->getChannel(),
            $dlq->getName(),
        );
        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }

    private function whenPruneRequestIsMadeForChannel(Channel $channel): void
    {
        try {
            $this->summaries[] = $this->deadLetterQueueService->pruneDlqChannel(
                $this->pruneRequestDto,
                $channel->getName(),
            );
        } catch (Exception $e) {
            $this->caughtException = $e;
        }
    }

    private function whenPruneRequestIsMadeForAllDlqChannels(): void
    {
        try {
            $this->summaries = $this->deadLetterQueueService->pruneAllDlqChannels($this->pruneRequestDto);
        } catch (Exception $e) {
            $this->caughtException = $e;
        }
    }

    private function whenSummaryIsGeneratedForChannel(Channel $channel): void
    {
        try {
            $this->summaries[] = $this->deadLetterQueueService->getSummary($channel->getName());
        } catch (Exception $e) {
            $this->caughtException = $e;
        }
    }

    private function whenSummariesAreGeneratedForAllDlqChannels(): void
    {
        try {
            $this->summaries = $this->deadLetterQueueService->getSummariesForAllDlqChannels();
        } catch (Exception $e) {
            $this->caughtException = $e;
        }
    }

    private function expectPayloadIdToBeRequeued(): void
    {
        self::assertSame(
            $this->pruneRequestDto->payloadId,
            $this->pruningService->getPrunedPayloadIds(PruningOperation::REQUEUE)[0]
        );
    }

    private function expectPayloadIdToBeDeleted(): void
    {
        self::assertSame(
            $this->pruneRequestDto->payloadId,
            $this->pruningService->getPrunedPayloadIds(PruningOperation::DELETE)[0]
        );
    }

    private function expectChannelsToBeRequeued(): void
    {
        self::assertSame(
            $this->pruneRequestDto->channels,
            $this->pruningService->getPrunedChannels(PruningOperation::REQUEUE)
        );
    }

    private function expectChannelsToBeDeleted(): void
    {
        self::assertSame(
            $this->pruneRequestDto->channels,
            $this->pruningService->getPrunedChannels(PruningOperation::DELETE)
        );
    }

    private function expectTypesToBeRequeued(): void
    {
        self::assertSame(
            $this->pruneRequestDto->types,
            $this->pruningService->getPrunedTypes(PruningOperation::REQUEUE)
        );
    }

    private function expectTypesToBeDeleted(): void
    {
        self::assertSame(
            $this->pruneRequestDto->types,
            $this->pruningService->getPrunedTypes(PruningOperation::DELETE)
        );
    }

    private function expectSummary(Channel $channel, int $messageCount = 0): void
    {
        $summary = $this->findSummaryForChannel($channel);
        self::assertNotNull($summary);
        self::assertSame($channel->getName(), $summary->channel);
        self::assertSame($messageCount, $summary->total);
    }

    private function expectValidationException(): void
    {
        /** @var ValidationException $exception */
        $exception = $this->caughtException;
        self::assertInstanceOf(ValidationException::class, $exception);
        $this->caughtException = null;
    }

    private function findSummaryForChannel(Channel $channel): ?SummaryDto
    {
        foreach ($this->summaries as $summary) {
            if ($summary->channel === $channel->getName()) {
                return $summary;
            }
        }

        return null;
    }

    private function getDlq(string $channel): Channel
    {
        return new Channel($channel . $this->deadLetterQueueSuffix);
    }
}
