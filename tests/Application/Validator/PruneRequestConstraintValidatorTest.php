<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Application\Validator;

use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Talentry\MessageBrokerAdministration\Application\Dto\PruneRequestDto;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningOperation;

class PruneRequestConstraintValidatorTest extends KernelTestCase
{
    private const string VALID_CHANNEL_NAME = 'EVENTS';
    private const string INVALID_CHANNEL_NAME = 'foo';
    private const string VALID_TYPE = __CLASS__;
    private const string INVALID_TYPE = 'bar';

    private ValidatorInterface $validator;
    private PruneRequestDto $pruneRequestDto;
    private ConstraintViolationListInterface $violations;

    protected function setUp(): void
    {
        $this->validator = self::getContainer()->get(ValidatorInterface::class);
        $this->pruneRequestDto = new PruneRequestDto();
        $this->pruneRequestDto->operation = PruningOperation::DELETE->value;
    }

    #[DataProvider('dataProviderForTestWithInvalidOrMissingOperation')]
    public function testWithInvalidOrMissingOperation(PruneRequestDto $pruneRequestDto): void
    {
        $this->givenPruneRequestDto($pruneRequestDto);
        $this->whenValidationIsPerformed();
        $this->expectViolation('Invalid pruning operation. Supported values are: requeue, delete');
    }

    #[DataProvider('dataProviderForTestWithMoreThanOneFilter')]
    public function testWithMoreThanOneFilter(PruneRequestDto $pruneRequestDto): void
    {
        $this->givenPruneRequestDto($pruneRequestDto);
        $this->whenValidationIsPerformed();
        $this->expectViolation('You must specify either payloadId, types or channels.');
    }

    public function testWithInvalidType(): void
    {
        $this->givenPruneRequestDtoWithTypes([self::INVALID_TYPE]);
        $this->whenValidationIsPerformed();
        $this->expectViolation('Invalid type provided: ' . self::INVALID_TYPE, 'types');
    }

    public function testWithPartiallyValidChannelsAndSkipInvalidFlag(): void
    {
        $this->givenPruneRequestDtoWithChannels([self::INVALID_CHANNEL_NAME, self::VALID_CHANNEL_NAME]);
        $this->givenPruneRequestDtoWithSkipInvalidFlag();
        $this->whenValidationIsPerformed();
        $this->expectNoViolations();
    }

    public function testWithPartiallyValidTypesAndSkipInvalidFlag(): void
    {
        $this->givenPruneRequestDtoWithTypes([self::INVALID_TYPE, self::VALID_TYPE]);
        $this->givenPruneRequestDtoWithSkipInvalidFlag();
        $this->whenValidationIsPerformed();
        $this->expectNoViolations();
    }

    #[DataProvider('dataProviderForTestWithValidDto')]
    public function testWithValidDto(PruneRequestDto $pruneRequestDto): void
    {
        $this->givenPruneRequestDto($pruneRequestDto);
        $this->whenValidationIsPerformed();
        $this->expectNoViolations();
    }

    private function givenPruneRequestDto(PruneRequestDto $pruneRequestDto): void
    {
        $this->pruneRequestDto = $pruneRequestDto;
    }

    private function givenPruneRequestDtoWithChannels(array $channels): void
    {
        $this->pruneRequestDto->channels = $channels;
    }

    private function givenPruneRequestDtoWithTypes(array $types): void
    {
        $this->pruneRequestDto->types = $types;
    }

    private function givenPruneRequestDtoWithSkipInvalidFlag(): void
    {
        $this->pruneRequestDto->skipInvalid = true;
    }

    private function whenValidationIsPerformed(): void
    {
        $this->violations = $this->validator->validate($this->pruneRequestDto);
    }

    private function expectViolation(string $message, ?string $path = null): void
    {
        self::assertCount(1, $this->violations);
        $violation = $this->violations->get(0);
        self::assertSame($message, $violation->getMessage());
        if ($path !== null) {
            self::assertSame($path, $violation->getPropertyPath());
        }
    }

    private function expectNoViolations(): void
    {
        self::assertCount(0, $this->violations);
    }

    public static function dataProviderForTestWithMoreThanOneFilter(): array
    {
        $dto1 = new PruneRequestDto();
        $dto1->operation = PruningOperation::DELETE->value;
        $dto1->payloadId = '1';
        $dto1->channels = [self::VALID_CHANNEL_NAME];

        $dto2 = new PruneRequestDto();
        $dto2->operation = PruningOperation::DELETE->value;
        $dto2->payloadId = '1';
        $dto2->types = [self::VALID_TYPE];

        $dto3 = new PruneRequestDto();
        $dto3->operation = PruningOperation::DELETE->value;
        $dto3->channels = [self::VALID_CHANNEL_NAME];
        $dto3->types = [self::VALID_TYPE];

        return [
            [$dto1],
            [$dto2],
            [$dto3],
        ];
    }

    public static function dataProviderForTestWithValidDto(): array
    {
        $dto1 = new PruneRequestDto();
        $dto1->operation = PruningOperation::DELETE->value;
        $dto1->payloadId = '1';

        $dto2 = new PruneRequestDto();
        $dto2->operation = PruningOperation::DELETE->value;
        $dto2->types = [self::VALID_TYPE];

        $dto3 = new PruneRequestDto();
        $dto3->operation = PruningOperation::DELETE->value;
        $dto3->channels = [self::VALID_CHANNEL_NAME];

        return [
            [$dto1],
            [$dto2],
            [$dto3],
        ];
    }

    public static function dataProviderForTestWithInvalidOrMissingOperation(): array
    {
        $dto1 = new PruneRequestDto();
        $dto1->payloadId = '1';

        $dto2 = new PruneRequestDto();
        $dto2->operation = 'foo';
        $dto2->payloadId = '1';

        return [
            [$dto1],
            [$dto2],
        ];
    }
}
