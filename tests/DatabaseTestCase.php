<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class DatabaseTestCase extends KernelTestCase
{
    protected EntityManagerInterface $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->getService(EntityManagerInterface::class);
        $schemaTool = new SchemaTool($this->entityManager);
        $schemaTool->dropDatabase();
        $schemaTool->createSchema($this->entityManager->getMetadataFactory()->getAllMetadata());
    }

    /**
     * @template T
     *
     * @param class-string<T> $serviceId
     *
     * @return T
     */
    protected function getService(string $serviceId)
    {
        return self::getContainer()->get($serviceId);
    }
}
