<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Domain\MessageRequeuer;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Infrastructure\ArrayMessageBroker;
use Talentry\MessageBrokerAdministration\Infrastructure\MessageRequeuer\FallbackMessageRequeuer;
use Talentry\MessageBrokerAdministration\Tests\MessageBrokerGivens;

class MessageRequeuerTest extends TestCase
{
    private MessageBrokerGivens $givens;

    protected function setUp(): void
    {
        $broker = new ArrayMessageBroker();
        $this->givens = new MessageBrokerGivens(
            $broker,
            $broker,
            new FallbackMessageRequeuer($broker),
        );
    }

    public function testRequeuingWithArbitraryType(): void
    {
        $this->givens->givenThatArbitraryMessageIsDispatched();
        $this->givens->whenMessageIsFetched();
        $this->givens->expectNoMessagesInQueue();

        $this->givens->whenMessageIsRequeued();
        $this->givens->expectMessageInQueue();
    }
}
