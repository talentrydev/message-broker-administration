<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Domain\Pruning;

use PHPUnit\Framework\Attributes\DataProvider;
use stdClass;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Domain\Mapper\MessageMapper;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningOperation;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningService;
use Talentry\MessageBrokerAdministration\Infrastructure\Repository\MessageRepository;
use Talentry\MessageBrokerAdministration\Tests\DatabaseTestCase;
use Talentry\MessageBrokerAdministration\Tests\Mocks\MockMessageRequeuer;

class PruningServiceTest extends DatabaseTestCase
{
    private PruningService $pruningService;
    private MessageRepository $messageRepository;
    private MockMessageRequeuer $messageRequeuer;
    private ChannelRegistry $channelRegistry;
    private Message $message;
    private Channel $channel;
    private Channel $dlqChannel;
    private object $payload;

    protected function setUp(): void
    {
        parent::setUp();

        $this->messageRequeuer = new MockMessageRequeuer();
        $this->payload = new stdClass();
        $this->messageRepository = $this->getService(MessageRepository::class);
        $this->pruningService = new PruningService(
            $this->messageRepository,
            $this->messageRequeuer,
            new MessageMapper(),
            $this->entityManager,
        );
        $this->channelRegistry = $this->getService(ChannelRegistry::class);
    }

    #[DataProvider('channelProvider')]
    public function testRequeueByExistingPayloadId(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsRequeuedByExistingPayloadId();
        $this->expectMessageToBeRequeued();
        $this->expectMessageToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testDeleteByExistingPayloadId(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsDeletedByExistingPayloadId();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testRequeueByNonExistingPayloadId(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsRequeuedByNonExistingPayloadId();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageNotToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testDeleteByNonExistingPayloadId(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsDeletedByNonExistingPayloadId();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageNotToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testRequeueByExistingChannel(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsRequeuedByExistingChannel();
        $this->expectMessageToBeRequeued();
        $this->expectMessageToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testDeleteByExistingChannel(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsDeletedByExistingChannel();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testRequeueByNonExistingChannel(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsRequeuedByNonExistingChannel();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageNotToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testDeleteByNonExistingChannel(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsDeletedByNonExistingChannel();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageNotToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testRequeueByExistingType(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsRequeuedByExistingType();
        $this->expectMessageToBeRequeued();
        $this->expectMessageToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testDeleteByExistingType(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsDeletedByExistingType();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testRequeueByNonExistingType(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsRequeuedByNonExistingType();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageNotToBeDeleted();
    }

    #[DataProvider('channelProvider')]
    public function testDeleteByNonExistingType(Channel $channel): void
    {
        $this->givenChannel($channel);
        $this->givenThatMessageIsStored();
        $this->whenMessageIsDeletedByNonExistingType();
        $this->expectNoMessageToBeRequeued();
        $this->expectMessageNotToBeDeleted();
    }

    private function givenChannel(Channel $channel): void
    {
        $this->channel = $channel;
        $this->dlqChannel = new Channel($this->channelRegistry->getDeadLetterQueueNameForChannel($channel));
    }

    private function givenThatMessageIsStored(): void
    {
        $this->message = new Message(
            payloadId: uniqid(),
            payload: 'payload',
            channel: $this->channel->getName(),
            dlqChannel: $this->dlqChannel->getName(),
            type: $this->payload::class,
        );
        $this->entityManager->persist($this->message);
        $this->entityManager->flush();
    }

    private function whenMessageIsRequeuedByExistingPayloadId(): void
    {
        $this->pruningService->pruneByPayloadId(
            $this->message->getPayloadId(),
            PruningOperation::REQUEUE,
            $this->dlqChannel,
        );
    }

    private function whenMessageIsDeletedByExistingPayloadId(): void
    {
        $this->pruningService->pruneByPayloadId(
            $this->message->getPayloadId(),
            PruningOperation::DELETE,
            $this->dlqChannel,
        );
    }

    private function whenMessageIsRequeuedByNonExistingPayloadId(): void
    {
        $this->pruningService->pruneByPayloadId(
            uniqid(),
            PruningOperation::REQUEUE,
            $this->dlqChannel,
        );
    }

    private function whenMessageIsDeletedByNonExistingPayloadId(): void
    {
        $this->pruningService->pruneByPayloadId(uniqid(), PruningOperation::DELETE, $this->dlqChannel);
    }

    private function whenMessageIsRequeuedByExistingChannel(): void
    {
        $this->pruningService->pruneByChannel(
            [new Channel($this->message->getChannel())],
            PruningOperation::REQUEUE,
            $this->dlqChannel,
        );
    }

    private function whenMessageIsDeletedByExistingChannel(): void
    {
        $this->pruningService->pruneByChannel(
            [new Channel($this->message->getChannel())],
            PruningOperation::DELETE,
            $this->dlqChannel,
        );
    }

    private function whenMessageIsRequeuedByNonExistingChannel(): void
    {
        $this->pruningService->pruneByChannel([new Channel(uniqid())], PruningOperation::REQUEUE, $this->dlqChannel);
    }

    private function whenMessageIsDeletedByNonExistingChannel(): void
    {
        $this->pruningService->pruneByChannel([new Channel(uniqid())], PruningOperation::DELETE, $this->dlqChannel);
    }

    private function whenMessageIsRequeuedByExistingType(): void
    {
        $this->pruningService->pruneByType([$this->payload::class], PruningOperation::REQUEUE, $this->dlqChannel);
    }

    private function whenMessageIsDeletedByExistingType(): void
    {
        $this->pruningService->pruneByType([$this->payload::class], PruningOperation::DELETE, $this->dlqChannel);
    }

    private function whenMessageIsRequeuedByNonExistingType(): void
    {
        $this->pruningService->pruneByType([uniqid()], PruningOperation::REQUEUE, $this->dlqChannel);
    }

    private function whenMessageIsDeletedByNonExistingType(): void
    {
        $this->pruningService->pruneByType([uniqid()], PruningOperation::DELETE, $this->dlqChannel);
    }

    private function expectMessageToBeRequeued(): void
    {
        $requeuedMessages = $this->messageRequeuer->getRequeuedMessages();
        if (count($requeuedMessages) === 0) {
            self::fail('Message was not requeued');
        }

        foreach ($requeuedMessages as $message) {
            self::assertSame($message->getPayloadId(), $this->message->getPayloadId());
            self::assertSame($message->getPayload(), $this->message->getPayload());
            self::assertSame($message->getChannel()->getName(), $this->message->getChannel());
        }
    }

    private function expectNoMessageToBeRequeued(): void
    {
        self::assertCount(0, $this->messageRequeuer->getRequeuedMessages());
    }

    private function expectMessageToBeDeleted(): void
    {
        self::assertCount(0, $this->messageRepository->findByDlqChannel($this->dlqChannel->getName()));
    }

    private function expectMessageNotToBeDeleted(): void
    {
        $messages = $this->messageRepository->findByDlqChannel($this->dlqChannel->getName());
        self::assertCount(1, $messages);
        self::assertEquals($this->message, $messages[0]);
    }

    public static function channelProvider(): array
    {
        return [
            [new Channel('foo')],
        ];
    }
}
