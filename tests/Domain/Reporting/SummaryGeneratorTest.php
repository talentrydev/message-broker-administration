<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Domain\Reporting;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Domain\Reporting\Payload;
use Talentry\MessageBrokerAdministration\Domain\Reporting\Summary;
use Talentry\MessageBrokerAdministration\Domain\Reporting\SummaryGenerator;
use Talentry\MessageBrokerAdministration\Infrastructure\Repository\MessageRepository;

class SummaryGeneratorTest extends TestCase
{
    private SummaryGenerator $summaryGenerator;
    private string $tenantId = '1';
    private string $messageType = 'Foo\Bar';
    private string $messagePayload = 'payload';
    private string $kibanaLink = 'https://example.org';

    protected function setUp(): void
    {
        $messageParser = $this->createMock(MessageParser::class);
        $messageRepository = $this->createMock(MessageRepository::class);
        $this->summaryGenerator = new SummaryGenerator(
            $messageParser,
            $messageRepository,
        );

        $messageRepository->method('findByDlqChannel')->willReturnCallback(
            function (string $channel): array {
                return [new Message(uniqid(), 'foo', $channel, $channel . '_DLQ')];
            }
        );
        $messageParser->method('getTenantId')->willReturn($this->tenantId);
        $messageParser->method('getMessageType')->willReturn($this->messageType);
        $messageParser->method('parse')->willReturn($this->messagePayload);
        $messageParser->method('getKibanaLink')->willReturn($this->kibanaLink);
    }

    public function testForChannel(): void
    {
        $channel = new Channel('foo');
        $summary = $this->summaryGenerator->forDlqChannel($channel);
        $this->assertValidSummaryForDlqChannel($channel, $summary);
    }

    private function assertValidSummaryForDlqChannel(Channel $dlqChannel, Summary $summary): void
    {
        self::assertSame($dlqChannel->getName(), $summary->dlqChannel()->getName());
        self::assertSame(1, $summary->total());
        self::assertSame([$dlqChannel->getName() => 1], $summary->perChannel());
        self::assertSame([$this->tenantId => 1], $summary->perTenant());
        self::assertSame([$this->messageType => 1], $summary->perType());
        self::assertCount(1, $summary->payloads());
        $payload = $summary->payloads()[0];
        self::assertInstanceOf(Payload::class, $payload);
        self::assertSame($this->messagePayload, $payload->payload());
        self::assertSame($this->messagePayload, $payload->payload());
        self::assertSame($this->kibanaLink, $payload->kibanaLink());
    }
}
