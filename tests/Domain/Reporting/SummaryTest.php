<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Domain\Reporting;

use PHPUnit\Framework\TestCase;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBrokerAdministration\Domain\Reporting\Payload;
use Talentry\MessageBrokerAdministration\Domain\Reporting\Summary;
use Talentry\Slack\Channel as SlackChannel;
use Talentry\Slack\SlackClient;

class SummaryTest extends TestCase
{
    private ?Summary $summary;
    private Channel $dlqChannel;
    private string $messageType = 'Foo\Bar';
    private SlackClient $slackClient;
    private Payload $payload;
    private string $tenantId = '1';
    private string $slackChannelName = 'general';
    private string $slackChannelId = 'C0000001';

    protected function setUp(): void
    {
        $this->dlqChannel = new Channel('foo');
        $this->payload = new Payload(
            '36b72d8444f9',
            'payload',
            'foo'
        );

        $this->summary = new Summary(
            $this->dlqChannel,
            1,
            [$this->dlqChannel->getName() => 1],
            [$this->messageType => 1],
            [$this->tenantId => 1],
            [$this->payload],
        );
        $this->slackClient = $this->createMock(SlackClient::class);

        $this->slackClient
            ->method('getChannel')
            ->with($this->slackChannelName)
            ->willReturn(new SlackChannel($this->slackChannelId, $this->slackChannelName))
        ;
    }

    public function testToString(): void
    {
        $expectedString = "SUMMARY FOR DLQ CHANNEL {$this->dlqChannel->getName()}:\n";
        $expectedString .= "TOTAL: 1\nPER CHANNEL:\n\t{$this->dlqChannel->getName()}: 1\n";
        $expectedString .= "PER TYPE:\n\t" . $this->messageType . ": 1\n";
        $expectedString .= "PER TENANT:\n\t{$this->tenantId}: 1\n";

        self::assertSame($expectedString, (string) $this->summary);
    }

    public function testDumpPayloadsToFile(): void
    {
        $file = $this->summary->dumpPayloadsToFile();
        $expectedContents = print_r($this->payload, true);
        self::assertSame($expectedContents, file_get_contents($file->getRealPath()));
    }

    public function testPostToSlack(): void
    {
        $uploadedFileUrl = 'http://example.org';

        $this->slackClient
            ->expects(self::once())
            ->method('sendFile')
        ;

        $this->slackClient
            ->method('sendPlainTextMessage')
            ->willReturnCallback(function (string $message, SlackChannel $slackChanel) use ($uploadedFileUrl) {
                self::assertSame($this->slackChannelName, $slackChanel->name());
                self::assertSame($this->slackChannelId, $slackChanel->id());
                $expectedSlackMessage = $this->summary . "\n" . $uploadedFileUrl;
                self::assertSame($expectedSlackMessage, $message);
            })
        ;

        $this->summary->postToSlack($this->slackClient, $this->slackChannelName);
    }
}
