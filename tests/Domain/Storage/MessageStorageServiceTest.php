<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Domain\Storage;

use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBrokerAdministration\Domain\Repository\MessageRepositoryInterface;
use Talentry\MessageBrokerAdministration\Domain\Storage\MessageStorageService;
use Talentry\MessageBrokerAdministration\Tests\DatabaseTestCase;

class MessageStorageServiceTest extends DatabaseTestCase
{
    private MessageStorageService $service;
    private MessagePublisher $messagePublisher;
    private MessageSubscriber $messageSubscriber;
    private MessageRepositoryInterface $messageRepository;
    private ChannelRegistry $channelRegistry;
    private Channel $channel;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->getService(MessageStorageService::class);
        $this->messagePublisher = $this->getService(MessagePublisher::class);
        $this->messageSubscriber = $this->getService(MessageSubscriber::class);
        $this->messageRepository = $this->getService(MessageRepositoryInterface::class);
        $this->channel = new Channel(uniqid());
        $this->channelRegistry = $this->getService(ChannelRegistry::class);
        $this->channelRegistry->registerChannel($this->channel);
    }

    public function testStoreMessagesFromChannel(): void
    {
        $message = new Message($this->channel, uniqid(), uniqid());

        $this->messagePublisher->sendMessage($message);
        $this->service->storeMessagesInDlqChannel($message->getChannel());

        $messageEntity = $this->messageRepository->findByPayloadId($message->getPayloadId());
        self::assertSame($message->getChannel()->getName(), $messageEntity->getChannel());
        self::assertSame($message->getPayload(), $messageEntity->getPayload());

        $message = $this->messageSubscriber->getMessageFromChannel($message->getChannel());
        self::assertNull($message);
    }

    public function testStoreMessagesFromAllDlqChannels(): void
    {
        $message1 = new Message($this->channel, uniqid(), uniqid());
        //we're sending message2 directly to the DLQ channel
        $dlqChannel = new Channel($this->channelRegistry->getDeadLetterQueueNameForChannel($this->channel));
        $message2 = new Message($dlqChannel, uniqid(), uniqid());

        $this->channelRegistry->registerChannel($message1->getChannel());
        $this->channelRegistry->registerChannel($message2->getChannel());

        $this->messagePublisher->sendMessage($message1);
        $this->messagePublisher->sendMessage($message2);

        $this->service->storeMessagesInAllDlqChannels();

        $messageEntity = $this->messageRepository->findByPayloadId($message1->getPayloadId());
        self::assertNull($messageEntity);

        $messageEntity = $this->messageRepository->findByPayloadId($message2->getPayloadId());
        self::assertSame($message2->getChannel()->getName(), $messageEntity->getChannel());
        self::assertSame($message2->getPayload(), $messageEntity->getPayload());

        $message = $this->messageSubscriber->getMessageFromChannel($message1->getChannel());
        self::assertEquals($message1, $message);

        $message = $this->messageSubscriber->getMessageFromChannel($message1->getChannel());
        self::assertNull($message);
    }
}
