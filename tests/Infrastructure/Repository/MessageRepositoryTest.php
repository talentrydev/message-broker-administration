<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Infrastructure\Repository;

use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Infrastructure\Repository\MessageRepository;
use Talentry\MessageBrokerAdministration\Tests\DatabaseTestCase;

class MessageRepositoryTest extends DatabaseTestCase
{
    private MessageRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->getService(MessageRepository::class);
    }

    public function testFindById(): void
    {
        $id = uniqid();
        $payload = 'payload';
        $channel = 'channel';

        $this->givenMessageExists(new Message($id, $payload, $channel, $channel . '_DLQ'));

        $message = $this->repository->findByPayloadId($id);
        self::assertSame($id, $message->getPayloadId());
        self::assertSame($payload, $message->getPayload());
        self::assertSame($channel, $message->getChannel());

        $message = $this->repository->findByPayloadId('foo');
        self::assertNull($message);
    }

    public function testFindByDlqChannel(): void
    {
        $message1 = new Message(uniqid(), uniqid(), 'c1', 'c1_DLQ');
        $message2 = new Message(uniqid(), uniqid(), 'c1', 'c1_DLQ');
        $message3 = new Message(uniqid(), uniqid(), 'c2', 'c2_DLQ');
        $this->givenMessageExists($message1);
        $this->givenMessageExists($message2);
        $this->givenMessageExists($message3);

        $messages = $this->repository->findByDlqChannel('c1_DLQ');
        self::assertCount(2, $messages);
        self::assertSame($message1->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message1->getPayload(), $messages[0]->getPayload());
        self::assertSame($message2->getPayloadId(), $messages[1]->getPayloadId());
        self::assertSame($message2->getPayload(), $messages[1]->getPayload());

        $messages = $this->repository->findByDlqChannel('c2_DLQ');
        self::assertCount(1, $messages);
        self::assertSame($message3->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message3->getPayload(), $messages[0]->getPayload());

        $messages = $this->repository->findByDlqChannel('c3_DLQ');
        self::assertCount(0, $messages);

        //test limit
        $messages = $this->repository->findByDlqChannel(dlqChannel: 'c1_DLQ', limit: 1);
        self::assertCount(1, $messages);
    }

    public function testFindByOriginChannel(): void
    {
        $message1 = new Message(uniqid(), uniqid(), 'c1', 'c1_DLQ');
        $message2 = new Message(uniqid(), uniqid(), 'c1', 'c1_DLQ');
        $message3 = new Message(uniqid(), uniqid(), 'c2', 'c2_DLQ');
        $this->givenMessageExists($message1);
        $this->givenMessageExists($message2);
        $this->givenMessageExists($message3);

        $messages = $this->repository->findByOriginChannel('c1');
        self::assertCount(2, $messages);
        self::assertSame($message1->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message1->getPayload(), $messages[0]->getPayload());
        self::assertSame($message2->getPayloadId(), $messages[1]->getPayloadId());
        self::assertSame($message2->getPayload(), $messages[1]->getPayload());

        $messages = $this->repository->findByOriginChannel('c2');
        self::assertCount(1, $messages);
        self::assertSame($message3->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message3->getPayload(), $messages[0]->getPayload());

        $messages = $this->repository->findByOriginChannel(['c1', 'c2']);
        self::assertCount(3, $messages);
        self::assertSame($message1->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message1->getPayload(), $messages[0]->getPayload());
        self::assertSame($message2->getPayloadId(), $messages[1]->getPayloadId());
        self::assertSame($message2->getPayload(), $messages[1]->getPayload());
        self::assertSame($message3->getPayloadId(), $messages[2]->getPayloadId());
        self::assertSame($message3->getPayload(), $messages[2]->getPayload());

        $messages = $this->repository->findByOriginChannel('c3');
        self::assertCount(0, $messages);

        //test limit
        $messages = $this->repository->findByOriginChannel(originChannels: 'c1', limit: 1);
        self::assertCount(1, $messages);
    }

    public function testFindByType(): void
    {
        $message1 = new Message(uniqid(), uniqid(), 'c1', 'c1_DLQ', 't1');
        $message2 = new Message(uniqid(), uniqid(), 'c1', 'c1_DLQ', 't1');
        $message3 = new Message(uniqid(), uniqid(), 'c2', 'c2_DLQ', 't2');
        $this->givenMessageExists($message1);
        $this->givenMessageExists($message2);
        $this->givenMessageExists($message3);

        $messages = $this->repository->findByType('t1');
        self::assertCount(2, $messages);
        self::assertSame($message1->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message1->getPayload(), $messages[0]->getPayload());
        self::assertSame($message2->getPayloadId(), $messages[1]->getPayloadId());
        self::assertSame($message2->getPayload(), $messages[1]->getPayload());

        $messages = $this->repository->findByType('t2');
        self::assertCount(1, $messages);
        self::assertSame($message3->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message3->getPayload(), $messages[0]->getPayload());

        $messages = $this->repository->findByType(['t1', 't2']);
        self::assertCount(3, $messages);
        self::assertSame($message1->getPayloadId(), $messages[0]->getPayloadId());
        self::assertSame($message1->getPayload(), $messages[0]->getPayload());
        self::assertSame($message2->getPayloadId(), $messages[1]->getPayloadId());
        self::assertSame($message2->getPayload(), $messages[1]->getPayload());
        self::assertSame($message3->getPayloadId(), $messages[2]->getPayloadId());
        self::assertSame($message3->getPayload(), $messages[2]->getPayload());

        $messages = $this->repository->findByType('t3');
        self::assertCount(0, $messages);

        //test limit
        $messages = $this->repository->findByType(types: 't1', limit: 1);
        self::assertCount(1, $messages);
    }

    private function givenMessageExists(Message $message): void
    {
        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }
}
