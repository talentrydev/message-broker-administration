<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests;

use PHPUnit\Framework\Assert;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Message\Message;
use Talentry\MessageBrokerAdministration\ApplicationInterface\MessageRequeuer;

class MessageBrokerGivens
{
    private Channel $channel;
    private ?Message $message = null;

    public function __construct(
        private readonly MessagePublisher $messagePublisher,
        private readonly MessageSubscriber $messageSubscriber,
        private readonly MessageRequeuer $messageRequeuer,
    ) {
    }

    public function givenThatArbitraryMessageIsDispatched(): void
    {
        $this->channel = new Channel('foo');
        $message = new Message($this->channel, 'foo', uniqid());
        $this->messagePublisher->sendMessage($message);
    }

    public function whenMessageIsFetched(): void
    {
        $this->message = $this->messageSubscriber->getMessageFromChannel($this->channel);
    }

    public function whenMessageIsRequeued(): void
    {
        $this->messageRequeuer->requeue($this->message);
    }

    public function expectMessageInQueue(): void
    {
        $message = $this->messageSubscriber->getMessageFromChannel($this->channel);
        Assert::assertSame($this->message->getPayloadId(), $message->getPayloadId());
        Assert::assertSame($this->message->getPayload(), $message->getPayload());
        Assert::assertSame($this->message->getChannelName(), $message->getChannelName());
    }

    public function expectNoMessagesInQueue(): void
    {
        Assert::assertNull($this->messageSubscriber->getMessageFromChannel($this->channel));
    }
}
