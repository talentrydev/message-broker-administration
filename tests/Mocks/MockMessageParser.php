<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Mocks;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;

class MockMessageParser implements MessageParser
{
    public function __construct(
        private readonly object|string|null $payload = null,
    ) {
    }

    public function getTenantId(Message $message): ?string
    {
        return null;
    }

    public function getMessageType(Message $message): string
    {
        return is_object($this->payload) ? $this->payload::class : 'n/a';
    }

    public function supports(Message $message): bool
    {
        return true;
    }

    public function parse(Message $message): object|string
    {
        return $this->payload !== null ? $this->payload : $message->getPayload();
    }

    public function getKibanaLink(Message $message): string
    {
        return 'https://example.org';
    }
}
