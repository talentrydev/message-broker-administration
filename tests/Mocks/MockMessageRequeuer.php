<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Mocks;

use Talentry\MessageBroker\ApplicationInterface\Message\Message;
use Talentry\MessageBrokerAdministration\ApplicationInterface\MessageRequeuer;

class MockMessageRequeuer implements MessageRequeuer
{
    /**
     * @var Message[]
     */
    private array $requeuedMessages = [];

    public function requeue(Message $message): void
    {
        $this->requeuedMessages[] = $message;
    }

    /**
     * @return Message[]
     */
    public function getRequeuedMessages(): array
    {
        return $this->requeuedMessages;
    }

    public function supports(Message $message): bool
    {
        return true;
    }
}
