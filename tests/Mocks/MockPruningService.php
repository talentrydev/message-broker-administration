<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Mocks;

use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningOperation;
use Talentry\MessageBrokerAdministration\Domain\Pruning\PruningService;

class MockPruningService extends PruningService
{
    private array $payloadIds;
    private array $channels;
    private array $types;

    public function __construct(
        private array $messages = [],
    ) {
        $this->payloadIds = [
            PruningOperation::REQUEUE->value => [],
            PruningOperation::DELETE->value => [],
        ];
        $this->channels = [
            PruningOperation::REQUEUE->value => [],
            PruningOperation::DELETE->value => [],
        ];
        $this->types = [
            PruningOperation::REQUEUE->value => [],
            PruningOperation::DELETE->value => [],
        ];
    }

    public function pruneByPayloadId(
        string $payloadId,
        PruningOperation $operation,
        ?Channel $dlqChannel = null,
        ?int $limit = null,
    ): array {
        $this->payloadIds[$operation->value][] = $payloadId;

        return $this->messages;
    }

    public function pruneByChannel(
        array $originChannels,
        PruningOperation $operation,
        ?Channel $dlqChannel = null,
        ?int $limit = null,
    ): array {
        $channelNames = array_map(fn (Channel $channel): string => $dlqChannel->getName(), $originChannels);
        $this->channels[$operation->value] = array_merge($this->channels[$operation->value], $channelNames);

        return $this->messages;
    }

    public function pruneByType(
        array $types,
        PruningOperation $operation,
        ?Channel $dlqChannel = null,
        ?int $limit = null,
    ): array {
        $this->types[$operation->value] = array_merge($this->types[$operation->value], $types);

        return $this->messages;
    }

    public function getPrunedPayloadIds(PruningOperation $operation): array
    {
        return $this->payloadIds[$operation->value];
    }

    public function getPrunedChannels(PruningOperation $operation): array
    {
        return $this->channels[$operation->value];
    }

    public function getPrunedTypes(PruningOperation $operation): array
    {
        return $this->types[$operation->value];
    }
}
