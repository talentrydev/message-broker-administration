<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Presentation\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Domain\Reporting\Summary;
use Talentry\MessageBrokerAdministration\Domain\Reporting\SummaryGenerator;
use Talentry\MessageBrokerAdministration\Presentation\Command\GenerateChannelSummaryCommand;
use Talentry\Slack\Channel as SlackChannel;
use Talentry\Slack\SlackClient;

class GenerateChannelSummaryCommandTest extends TestCase
{
    private CommandTester $commandTester;
    private SummaryGenerator $summaryGenerator;
    private Summary $summary;
    private SlackClient $slackClient;
    private ChannelRegistry $channelRegistry;
    private string $channelName = 'foo';
    private string $deadLetterQueueSuffix = '_dead_letter_queue';
    private string $dlqChannelName;

    protected function setUp(): void
    {
        $this->summaryGenerator = $this->createMock(SummaryGenerator::class);
        $this->summary = $this->createMock(Summary::class);
        $this->slackClient = $this->createMock(SlackClient::class);
        $this->channelRegistry = new ChannelRegistry($this->deadLetterQueueSuffix);

        $command = new GenerateChannelSummaryCommand(
            $this->summaryGenerator,
            $this->slackClient,
            $this->channelRegistry,
        );
        $this->commandTester = new CommandTester($command);
        $this->dlqChannelName = $this->channelRegistry->getDeadLetterQueueNameForChannel(
            new Channel($this->channelName),
        );
    }

    public function testCommandForOneChannelWithPostToSlackOption(): void
    {
        $this->givenThatSlackChannelExists();
        $this->summaryGenerator
            ->expects(self::once())
            ->method('forDlqChannel')
            ->willReturnCallback(function (Channel $channel, bool $includePerTenantSummary) {
                self::assertSame($this->dlqChannelName, $channel->getName());
                self::assertTrue($includePerTenantSummary);

                return $this->summary;
            });

        $this->summary
            ->expects(self::once())
            ->method('postToSlack')
            ->with($this->slackClient, 'general');

        $this->commandTester->execute([
            '--dlq-channel' => $this->dlqChannelName,
            '--post-to-slack' => true,
        ]);

        self::assertSame(0, $this->commandTester->getStatusCode());
    }

    public function testCommandForOneChannelWithoutPostToSlackOption(): void
    {
        $this->givenThatSlackChannelExists();
        $this->summaryGenerator
            ->expects(self::once())
            ->method('forDlqChannel')
            ->willReturnCallback(function (Channel $channel, bool $includePerTenantSummary) {
                self::assertSame($this->dlqChannelName, $channel->getName());
                self::assertTrue($includePerTenantSummary);

                return $this->summary;
            });

        $this->summary
            ->expects(self::never())
            ->method('postToSlack');

        $this->commandTester->execute([
            '--dlq-channel' => $this->dlqChannelName,
        ]);

        self::assertSame(0, $this->commandTester->getStatusCode());
        self::assertStringContainsString('Payloads dumped to:', $this->commandTester->getDisplay());
    }

    public function testCommandForAllChannelsWithoutPostToSlackOption(): void
    {
        $this->givenThatSlackChannelExists();
        $channel1 = new Channel('first');
        $channel2 = new Channel('second');
        $dlq1 = new Channel('first' . $this->deadLetterQueueSuffix);
        $dlq2 = new Channel('second' . $this->deadLetterQueueSuffix);
        $this->channelRegistry->registerChannel($channel1);
        $this->channelRegistry->registerChannel($channel2);

        $matcher = self::exactly(2);
        $this->summaryGenerator
            ->expects($matcher)
            ->method('forDlqChannel')
            ->willReturnCallback(
                function (Channel $dlqChannel, bool $includePerTenantSummary) use ($matcher, $dlq1, $dlq2) {
                    match ($matcher->numberOfInvocations()) {
                        1 => self::assertEquals($dlqChannel, $dlq1),
                        2 => self::assertEquals($dlqChannel, $dlq2),
                    };
                    self::assertSame(true, $includePerTenantSummary);

                    return $this->summary;
                }
            )
        ;

        $this->summary
            ->expects(self::never())
            ->method('postToSlack');

        $this->commandTester->execute([]);

        self::assertSame(0, $this->commandTester->getStatusCode());
        self::assertStringContainsString('Payloads dumped to:', $this->commandTester->getDisplay());
    }

    public function testCommandWithInvalidSlackChannel(): void
    {
        $this->givenThatSlackChannelDoesNotExist();

        $this->summary
            ->expects(self::never())
            ->method('postToSlack')
            ->with($this->slackClient, 'general');

        $this->commandTester->execute([
            '--dlq-channel' => $this->dlqChannelName,
            '--post-to-slack' => true,
        ]);

        self::assertSame(1, $this->commandTester->getStatusCode());
        self::assertSame('Invalid slack channel: general' . PHP_EOL, $this->commandTester->getDisplay());
    }

    public function testCommandWithInvalidSlackChannelAndPostToSlackDeactivated(): void
    {
        $this->givenThatSlackChannelDoesNotExist();

        $this->summary
            ->expects(self::never())
            ->method('postToSlack')
            ->with($this->slackClient, 'general');

        $this->commandTester->execute([
            '--dlq-channel' => $this->dlqChannelName,
        ]);

        self::assertSame(0, $this->commandTester->getStatusCode());
    }

    private function givenThatSlackChannelExists(): void
    {
        $this->slackClient
            ->method('getChannel')
            ->willReturn($this->createMock(SlackChannel::class));
    }

    private function givenThatSlackChannelDoesNotExist(): void
    {
        $this->slackClient
            ->method('getChannel')
            ->willReturn(null);
    }
}
