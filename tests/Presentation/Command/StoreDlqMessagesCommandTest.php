<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Presentation\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Talentry\MessageBrokerAdministration\Domain\Storage\MessageStorageService;
use Talentry\MessageBrokerAdministration\Presentation\Command\StoreDlqMessagesCommand;

class StoreDlqMessagesCommandTest extends TestCase
{
    private CommandTester $commandTester;
    private MessageStorageService $messageStorageService;

    protected function setUp(): void
    {
        $this->messageStorageService = $this->createMock(MessageStorageService::class);
        $command = new StoreDlqMessagesCommand($this->messageStorageService);
        $this->commandTester = new CommandTester($command);
    }

    public function testCommand(): void
    {
        $this->messageStorageService
            ->expects(self::once())
            ->method('storeMessagesInAllDlqChannels')
        ;

        $this->commandTester->execute([]);

        self::assertSame(0, $this->commandTester->getStatusCode());
    }
}
