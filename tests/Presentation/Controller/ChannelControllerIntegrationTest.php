<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Presentation\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Security\Core\User\InMemoryUser;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBrokerAdministration\Domain\Entity\Message;
use Talentry\MessageBrokerAdministration\Tests\DatabaseTestCase;

class ChannelControllerIntegrationTest extends DatabaseTestCase
{
    private KernelBrowser $kernelBrowser;
    private string $channelName = 'foo';
    private string $dlqName = 'foo_DLQ';
    private string $messagePayload = 'payload';
    private string $messagePayloadId;

    protected function setUp(): void
    {
        parent::setUp();

        $this->kernelBrowser = $this->getService('test.client');
        $channelRegistry = $this->getService(ChannelRegistry::class);
        $channelRegistry->registerChannel(new Channel($this->channelName));
        $this->messagePayloadId = uniqid();
        $message = new Message($this->messagePayloadId, $this->messagePayload, $this->channelName, $this->dlqName);
        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }

    public function testSummaryEndpoint(): void
    {
        $this->givenThatAdminIsAuthenticated();
        $this->whenSummaryEndpointIsCalled();
        $this->expectOkResponse();
        $this->expectSummaryForChannelInResponse();
    }

    public function testSummariesForAllDlqChannelsEndpoint(): void
    {
        $this->givenThatAdminIsAuthenticated();
        $this->whenSummariesForAllDlqChannelsEndpointIsCalled();
        $this->expectOkResponse();
        $this->expectSummariesForAllDlqChannelsInResponse();
    }

    public function testPruneEndpointWithValidPayload(): void
    {
        $this->givenThatAdminIsAuthenticated();
        $this->whenPruneEndpointIsCalledWithValidPayload();
        $this->expectOkResponse();
        $this->expectSummaryForChannelInResponse(expectEmptySummary: true);
    }

    public function testPruneEndpointWithInvalidPayload(): void
    {
        $this->givenThatAdminIsAuthenticated();
        $this->whenPruneEndpointIsCalledWithInvalidPayload();
        $this->expectBadRequestResponse();
    }

    public function testPruneAllDlqChannelsEndpoint(): void
    {
        $this->givenThatAdminIsAuthenticated();
        $this->whenPruneAllDlqChannelsEndpointIsCalled();
        $this->expectOkResponse();
        $this->expectSummariesForAllDlqChannelsInResponse(expectEmptySummary: true);
    }

    public function testInsufficientPermissions(): void
    {
        $this->givenThatNonAdminIsAuthenticated();
        $this->whenSummaryEndpointIsCalled();
        $this->expectForbiddenResponse();
    }

    private function givenThatNonAdminIsAuthenticated(): void
    {
        $this->kernelBrowser->loginUser(new InMemoryUser('username', 'password', ['ROLE_USER']));
    }

    private function givenThatAdminIsAuthenticated(): void
    {
        $this->kernelBrowser->loginUser(new InMemoryUser('username', 'password', ['ROLE_ADMIN']));
    }

    private function whenSummaryEndpointIsCalled(): void
    {
        $this->kernelBrowser->request(
            method: 'GET',
            uri: '/api/v1/admin/message-broker/summary/' . $this->dlqName,
        );
    }

    private function whenSummariesForAllDlqChannelsEndpointIsCalled(): void
    {
        $this->kernelBrowser->request(
            method: 'GET',
            uri: '/api/v1/admin/message-broker/summary',
        );
    }

    private function whenPruneEndpointIsCalledWithValidPayload(): void
    {
        $this->kernelBrowser->request(
            method: 'POST',
            uri: '/api/v1/admin/message-broker/prune/' . $this->dlqName,
            server: ['CONTENT_TYPE' => 'application/json'],
            content: json_encode(['operation' => 'delete', 'channels' => [$this->channelName]]),
        );
    }

    private function whenPruneEndpointIsCalledWithInvalidPayload(): void
    {
        $this->kernelBrowser->request(
            method: 'POST',
            uri: '/api/v1/admin/message-broker/prune/' . $this->dlqName,
            server: ['CONTENT_TYPE' => 'application/json'],
            content: json_encode(['operation' => 'foo'])
        );
    }

    private function whenPruneAllDlqChannelsEndpointIsCalled(): void
    {
        $this->kernelBrowser->request(
            method: 'POST',
            uri: '/api/v1/admin/message-broker/prune',
            server: ['CONTENT_TYPE' => 'application/json'],
            content: json_encode(['operation' => 'delete', 'channels' => [$this->channelName]]),
        );
    }

    private function expectOkResponse(): void
    {
        self::assertSame(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    private function expectSummaryForChannelInResponse(bool $expectEmptySummary = false): void
    {
        $content = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        $this->assertCorrectChannelSummary($content, $expectEmptySummary);
    }

    private function expectSummariesForAllDlqChannelsInResponse(bool $expectEmptySummary = false): void
    {
        $content = json_decode($this->kernelBrowser->getResponse()->getContent(), true);
        self::assertCount(1, $content);
        $this->assertCorrectChannelSummary($content[0], $expectEmptySummary);
    }

    private function expectForbiddenResponse(): void
    {
        self::assertSame(403, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    private function expectBadRequestResponse(): void
    {
        $response = $this->kernelBrowser->getResponse();
        self::assertSame(400, $response->getStatusCode());
    }

    private function assertCorrectChannelSummary(array $summary, bool $expectEmptySummary = false): void
    {
        self::assertSame($this->dlqName, $summary['channel']);
        self::assertSame($expectEmptySummary ? 0 : 1, $summary['total']);

        if (!$expectEmptySummary) {
            self::assertSame(['n/a' => 1], $summary['perTenant']);
            self::assertSame(['n/a' => 1], $summary['perType']);
            self::assertSame([$this->channelName => 1], $summary['perChannel']);
            self::assertSame([[
                'id' => $this->messagePayloadId,
                'payload' => $this->messagePayload,
                'kibanaLink' => 'https://example.org',
            ]], $summary['payloads']);
        }
    }
}
