<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerAdministration\Tests\Presentation\Controller;

use JMS\Serializer\SerializerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Talentry\MessageBroker\Application\Exception\ValidationException;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBrokerAdministration\Application\Dto\PruneRequestDto;
use Talentry\MessageBrokerAdministration\Application\Service\DeadLetterQueueService;
use Talentry\MessageBrokerAdministration\Presentation\Controller\ChannelController;

class ChannelControllerTest extends TestCase
{
    private ChannelController $channelController;
    private DeadLetterQueueService $deadLetterQueueService;
    private Response $response;
    private SerializerInterface $serializer;
    private Channel $channel;

    protected function setUp(): void
    {
        $this->deadLetterQueueService = $this->createMock(DeadLetterQueueService::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->serializer->method('serialize')->willReturn('foo');
        $this->channelController = new ChannelController(
            $this->deadLetterQueueService,
            $this->serializer,
        );
        $this->channel = new Channel('foo');
    }

    public function testSummaryEndpoint(): void
    {
        $this->whenSummaryEndpointIsCalled();
        $this->expectOkResponse();
    }

    public function testSummariesForAllDlqChannelsEndpoint(): void
    {
        $this->whenSummariesForAllDlqChannelsEndpointIsCalled();
        $this->expectOkResponse();
    }

    public function testPruneEndpointWithValidPayload(): void
    {
        $this->whenPruneEndpointIsCalled();
        $this->expectOkResponse();
    }

    public function testPruneEndpointWithInvalidPayload(): void
    {
        $this->givenValidationWillFail();
        $this->whenPruneEndpointIsCalled();
        $this->expectBadRequestResponse();
    }

    public function testPruneAllDlqChannelsEndpoint(): void
    {
        $this->whenPruneAllDlqChannelsEndpointIsCalled();
        $this->expectOkResponse();
    }

    private function givenValidationWillFail(): void
    {
        $this->deadLetterQueueService->method('pruneDlqChannel')->willThrowException(
            new ValidationException($this->createMock(ConstraintViolationListInterface::class))
        );
    }

    private function whenSummaryEndpointIsCalled(): void
    {
        $this->response = $this->channelController->summaryForDlqChannel(
            new Request(),
            $this->channel->getName(),
        );
    }

    private function whenSummariesForAllDlqChannelsEndpointIsCalled(): void
    {
        $this->response = $this->channelController->summariesForAllDlqChannels(new Request());
    }

    private function whenPruneEndpointIsCalled(): void
    {
        $postData = json_encode([
            'operation' => 'delete',
            'channel' => $this->channel->getName(),
        ]);

        $this->serializer
            ->method('deserialize')
            ->with($postData, PruneRequestDto::class, 'json')
            ->willReturn(new PruneRequestDto());

        $this->response = $this->channelController->pruneDlqChannel(
            new Request(content: $postData),
            $this->channel->getName(),
        );
    }

    private function whenPruneAllDlqChannelsEndpointIsCalled(): void
    {
        $postData = json_encode([
            'operation' => 'delete',
            'channel' => $this->channel->getName(),
        ]);

        $this->serializer
            ->method('deserialize')
            ->with($postData, PruneRequestDto::class, 'json')
            ->willReturn(new PruneRequestDto());

        $this->response = $this->channelController->pruneAllDlqChannels(new Request(content: $postData));
    }

    private function expectOkResponse(): void
    {
        self::assertSame(200, $this->response->getStatusCode());
    }

    private function expectBadRequestResponse(): void
    {
        self::assertSame(400, $this->response->getStatusCode());
    }
}
